package com.example.lynn.myarithmeticgame;

import android.content.Context;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;

import static com.example.lynn.myarithmeticgame.MainActivity.*;

/**
 * Created by lynn on 6/17/2016.
 */
public class MyView extends LinearLayout {

    public MyView(Context context) {
        super(context);

        setBackground(getResources().getDrawable(R.drawable.background));

        LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(200,200);

        operand1 = new TextView(context);

        operator = new TextView(context);

        operand2 = new TextView(context);

        answer = new EditText(context);

        play = new Button(context);

        play.setText("Play");

        submit = new Button(context);

        submit.setText("Submit");

        play.setOnClickListener(listener);

        submit.setOnClickListener(listener);

        scoreDisplay = new TextView(context);

        message = new TextView(context);

        operand1.setLayoutParams(layoutParams);
        operator.setLayoutParams(layoutParams);
        operand2.setLayoutParams(layoutParams);
        answer.setLayoutParams(layoutParams);
        play.setLayoutParams(layoutParams);
        submit.setLayoutParams(layoutParams);

        addView(operand1);
        addView(operator);
        addView(operand2);
        addView(answer);
        addView(play);
        addView(submit);
        addView(scoreDisplay);
        addView(message);
    }

}
